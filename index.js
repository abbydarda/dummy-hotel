const express = require('express')
const app = express()
const port = 8080
let faker = require('faker/locale/id_ID');

app.get('/traveler/nearby-hotel', (req, res) => {

    let next_start = parseInt(req.query.start) + parseInt(req.query.limit)
    let previous_start = parseInt(req.query.start) - parseInt(req.query.limit)

    previous_start = (previous_start < 0) ? 0 : previous_start
    
    let result = {
        status : 1,
        message : "rooms available",
        next_start : next_start,
        previous_start,
        total : 4000,
        result : []
    }
    
    for (let i = 0; i < 4000; i++) {

        result.result.push({
            index : i+1,
            hotel_id : faker.random.number(),
            name : faker.company.companyName(),
            promo_name : faker.commerce.productName(),
            distance : faker.random.number({min: 0, max: 3, precision:0.01}),
            lat : faker.address.latitude({min: 0, max: 1, precision:-6.230702}),
            lon : faker.address.longitude({min: 0, max: 1, precision:106.882744}),
            id : faker.random.number(),
            breakfast : 1,
            offer_id : faker.random.number(),
            created_at : faker.date.recent(),
            description : faker.lorem.sentences(),
            early_checkin : 1,
            expired_time : faker.date.future(),
            late_checkout : 1,
            pickup_service : 1,
            wifi : 1,
            qty : 30,
            regular_price : 250000,
            promo_price : 200000,
            stars : 5,
            pictures : [
                {
                    picfile : faker.image.imageUrl()
                },
                {
                    picfile : faker.image.imageUrl()
                }
            ]
        });
        
    }

    res.json(result);
    
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))